import sys
import re
import config

from xml.dom.minidom import parse, Node

class Station:
    def __init__(self, icao, lat, lng, elev, metar=None, taf=None, aloft=None):
        self.icao = icao
        self.latitude = lat
        self.longitude = lng
        self.elev = elev
        self.metar = metar
        self.taf = taf
        self.aloft = aloft

#station ICAO or "*" if not avbl
def getStationICAO(table):
    node1 = table.getElementsByTagName("station")
    if len(node1)==0:
        return "*"
    else:
        node1 = node1[0]
        if len(node1.childNodes) == 1: # tests if there is data
            return node1.childNodes[0].data
        else:
            return "*"

#station Metar or "*" if not avbl
def getStationMetar(table):
    node1 = table.getElementsByTagName("raw_text")
    if len(node1)==0:
        return "*"
    else:
        node1 = node1[0]
        if len(node1.childNodes) == 1: # tests if there is data
            return node1.childNodes[0].data
        else:
            return "*"

#station TAF or "*" if not avbl
def getStationTAF(table):
    node1 = table.getElementsByTagName("taf")
    if len(node1)==0:
        return "*"
    else:
        node1 = node1[0]
        if len(node1.childNodes) == 1: # tests if there is data
            return node1.childNodes[0].data
        else:
            return "*"

#station aloft or "*" if not avbl
def getStationAloft(table):
    aloftRet = ""
    aloftIdxs = ["upr925", "upr850", "upr700", "upr500", "upr400", "upr300", "upr250", "upr200"]

    node1 = table.getElementsByTagName("upr1000")
    if len(node1)==0:
        return "*"
    else:
        node1 = node1[0]
        if len(node1.childNodes) == 1: # tests if there is data
            aloft1 = node1.childNodes[0].data
            splitAloft = aloft1.split("|")
            wind = splitAloft[0]
            temp = splitAloft[1]
            aloft1 = wind[0:3]+","+wind[3:len(wind)]+","+temp
            aloftRet+=aloft1
        else:
            return "*"

    for idx in aloftIdxs:
        node1 = table.getElementsByTagName(idx)
        if len(node1)==0:
            return "*"
        else:
            node1 = node1[0]
            if len(node1.childNodes) == 1: # tests if there is data
                aloft1 = node1.childNodes[0].data
                splitAloft = aloft1.split("|")
                wind = splitAloft[0]
                temp = splitAloft[1]
                aloft1 = wind[0:3]+","+wind[3:len(wind)]+","+temp
                aloftRet = aloftRet+"/"+aloft1
            else:
                return "*"

    return aloftRet

def getStationCoords(table):
    node1 = table.getElementsByTagName("lat")
    if len(node1)==0:
        return 1000,1000
    else:
        node1 = node1[0]
        if len(node1.childNodes) == 1: # tests if there is data
            lat = float(node1.childNodes[0].data)
        else:
            return 1000,1000

    node1 = table.getElementsByTagName("lont")
    if len(node1)==0:
        return 1000,1000
    else:
        node1 = node1[0]
        if len(node1.childNodes) == 1: # tests if there is data
            lont = float(node1.childNodes[0].data)
        else:
            return 1000,1000
    return lat,lont


try:
    with open('stations.dat', 'r', encoding='UTF8') as file:            # context manager
        fileData = file.readlines()
    with open('wx_station_list_as.txt', 'r', encoding='UTF8') as file:  # context manager
        fileAS = file.readlines()
except IOError:
    sys.exit("Could not open station databases. Closing...")

stationData = {}
stationAS = {}

fileSnapshot = list()
fileStationList = list()

for line in fileData:
    line =  re.compile(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)").split(line)
    line = line[5:9]                                                    # slicing
    line = [item.replace('"', '') for item in line]                     # comprehension
    icao, lat, lng, elev = line                                         # unpacking
    stationData[icao] = Station(icao, lat, lng, elev)

for line in fileAS:
    icao, lat, lng, elev = line.split(',')
    stationAS[icao] = Station(icao, lat, lng, elev)

xmlTree = parse(config.REX_MET_REP_PATH + "\\"+'metar_report.xml')

for table in xmlTree.getElementsByTagName("Table"):
    stationICAO = getStationICAO(table)
    lat,lont = getStationCoords(table)
    if (lat==1000) or (lont==1000):
        if stationICAO in stationData:
            lat = stationData[stationICAO].latitude
            lont = stationData[stationICAO].longitude
            elev = stationData[stationICAO].elev
        elif stationICAO in stationAS:
            lat = stationAS[stationICAO].latitude
            lont = stationAS[stationICAO].longitude
            elev = stationAS[stationICAO].elev
        else:
            continue
    else:
        if stationICAO in stationData:
            elev = stationData[stationICAO].elev
        elif stationICAO in stationAS:
            elev = stationAS[stationICAO].elev
        else:
            continue

    icao = stationICAO
    metar = getStationMetar(table)
    taf = getStationTAF(table)
    aloft = getStationAloft(table)
    elev = int(elev)
    fileSnapshot.append(f'{icao}::{metar}::{taf}::{aloft}')
    fileStationList.append(f'{icao},{lat:.3f},{lont:.3f},{elev}')

try:
    if len(fileSnapshot) > 0:
        with open(f'{config.AS_OUTPUT_PATH}\\current_wx_snapshot.txt', 'w+', encoding='UTF8') as file:
            file.writelines(f'{line}\n' for line in fileSnapshot)

    if len(fileStationList) > 0:
        with open(f'{config.AS_OUTPUT_PATH}\\wx_station_list.txt', 'w+', encoding='UTF8') as file:
            file.writelines(f'{line}\n' for line in fileStationList)
except IOError:
    sys.exit("Could not write weather data. Closing...")
